public class AirFryer{
	
	private int maxTemp;
	private double volumeLitres;
	private String brand;
	
	public void setMaxTemp(int input){this.maxTemp = input;}
	public void setBrand(String input){this.brand = input;}
	
	public int getMaxTemp(){return this.maxTemp;}
	public double getVolumeLitres(){return this.volumeLitres;}
	public String getBrand(){return this.brand;}
	
	public AirFryer(int maxTemp, double volumeLitres, String brand){
		this.maxTemp = maxTemp;
		this.volumeLitres = volumeLitres;
		this.brand = brand;
	}
	
	public void canCook(int recipeTemp, double recipeVol){
		if (this.maxTemp>= recipeTemp && this.volumeLitres >= recipeVol){
			System.out.println("This recipe can be cooked in this air fryer!");
		}
		else{
			System.out.println("this recipie is incompatible with this air fryer");
		}
	}
	
	public void brag(){
		System.out.println("i as a "+brand+" brand airfryer am far superior to any other airfryer in the buisness.");
		System.out.println("my internal volume of "+volumeLitres+" is the perfect size, greater then all else");
		System.out.println("and my maximum temperature of "+maxTemp+" could scorch your foes to dust, or cook food requiring high temperatures. that aswell.");
	}
	
	public void printDetails(){
		System.out.println(brand);
		System.out.println("Max temperature of "+maxTemp+" mystery degrees");
		System.out.println("Internal volume of "+volumeLitres+" litres");
		
	}
	
	public void upgradeHeater(int input){
		if(greaterZero(input)==true){ 
			this.maxTemp += input;
			System.out.println(this.brand + " brand airfryer's maximum temperature was upgraded to " + this.maxTemp);
		}
	}
	
	private boolean greaterZero(int input){
		boolean out = false;
		if(input > 0){
			out = true;
		}
		return out;
	}
}